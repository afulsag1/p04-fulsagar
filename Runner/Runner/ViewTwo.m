#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>



@interface ViewTwo : UIViewController
{
    int a,b,count;
    float r;
    CGPoint startpoint,startpoint1,endpoint;
    CGPoint bl1,bl2,bl3,temp,temp1,temp2;
    UIView *track;
    CGRect screenRect;
    CGSize screenSize ;
    CGFloat screenWidth;
    CGFloat screenHeight;
    NSTimer * time,*time1,*time2,*score;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *runner;
@property (weak, nonatomic) IBOutlet UIImageView *runnerleft;
@property (weak, nonatomic) IBOutlet UIImageView *runnerjump;
@property (weak, nonatomic) IBOutlet UIImageView *runnerright;
@property (weak, nonatomic) IBOutlet UIImageView *block1;
@property (weak, nonatomic) IBOutlet UIImageView *block2;
@property (weak, nonatomic) IBOutlet UIImageView *block3;
@property (weak, nonatomic) IBOutlet UIImageView *fire;
@property (weak, nonatomic) IBOutlet UIButton *playagain;
@property (weak, nonatomic) IBOutlet UIImageView *explosion;
@property (weak, nonatomic) IBOutlet UILabel *textscore;



@end

@implementation ViewTwo
- (void)viewDidLoad {
    [super viewDidLoad];
    _runner.hidden=NO;
    count=0;
    screenRect = [[UIScreen mainScreen] bounds];
    screenSize = screenRect.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    _textscore.hidden=YES;
    _playagain.hidden=YES;
    _explosion.hidden=YES;
    
    [self trackgeneration];
    startpoint = _runnerleft.center;
    endpoint = _runnerright.center;
    a=1;
    bl1=_block1.center;
    bl2=_block2.center;
    bl3=_block3.center;
    wait(4);
     [self start];

    
}
-(void)trackgeneration
{
    //1st track
    track  = [[UIView alloc] initWithFrame:CGRectMake(20.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    
    [self.view addSubview:track];
    
    track  = [[UIView alloc] initWithFrame:CGRectMake(110.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    [self.view addSubview:track];
    
    //2nd track
    track  = [[UIView alloc] initWithFrame:CGRectMake(140.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    [self.view addSubview:track];
    
    track  = [[UIView alloc] initWithFrame:CGRectMake(230.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    [self.view addSubview:track];
    
    
    //3rd track
    track  = [[UIView alloc] initWithFrame:CGRectMake(250.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    [self.view addSubview:track];
    
    track  = [[UIView alloc] initWithFrame:CGRectMake(340.0, 10.0, 10.0, 700.0)];
    track.backgroundColor = [UIColor brownColor];
    [self.view addSubview:track];
    int x[5],y[6];
    x[0]=30;
    x[1]=150;
    x[2]=260;
    y[0]=100;
    //track application
    for(int i=1;i<6;i++)
    {
        y[i]=y[i-1]+100;
    }
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<6;j++)
        {
            track  = [[UIView alloc] initWithFrame:CGRectMake(x[i], y[j], 80.0, 10.0)];
            track.backgroundColor = [UIColor lightGrayColor];
            track.opaque=0.2;
            [self.view addSubview:track];
        }
    }

}

- (IBAction)swipeleft:(id)sender {
    startpoint.x=startpoint.x-110.0;
    endpoint.x=startpoint.x;
    _runnerleft.center=startpoint;
    _runnerright.center=startpoint;
}
- (IBAction)swiperight:(id)sender {
    startpoint.x=startpoint.x+110.0;
     endpoint.x=startpoint.x;
    _runnerleft.center=startpoint;
    _runnerright.center=startpoint;
}

- (IBAction)swipeup:(id)sender {
    _explosion.hidden=YES;
    startpoint1=_runnerleft.center;
    _fire.center=_runnerleft.center;
    endpoint=startpoint1;
    endpoint.y = endpoint.y-200.0;
    [UIView animateWithDuration:1
                     animations:^{
                         _fire.hidden=NO;
                         b=1;
                         _fire.center=endpoint;
                      }//animation ends here
                     completion:^(BOOL finished){
                         _fire.hidden=YES;
                         b=0;
                     }// completion ends here
     ];//uiview ends here

}
-(void)start
{
    //for runner
    time1=[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(hhh:) userInfo:NULL repeats:YES ];
    
    //for moving blockheads
     time2=[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(movingblock:) userInfo:NULL repeats:YES ];
    
    //for collision checking and block checking
   time= [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(blockchecking:) userInfo:NULL repeats:YES ];
    
    //update score
    score=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updatescore:) userInfo:NULL repeats:YES ];
}

-(void)blockchecking:timer
{
    NSString *string = [NSString stringWithFormat:@"%s%d","Your score is ", count];
    temp=_block1.center;
    r=arc4random_uniform(10);
    if(temp.y>700.0 )
    { //checking if block1 has reached bottom of screen
        bl1.y=20.0-(r*100);
        _block1.center=bl1;
    }
   if((_runnerleft.center.y-temp.y)<=20 && _runnerleft.center.x<=110 && _runnerleft.center.x>=20)
    {// collision checking of runner and block1
        _textscore.hidden=NO;
        _textscore.text=string;
        [time invalidate];
    [time1 invalidate];
    [time2 invalidate];
        [score invalidate];
        _playagain.hidden=NO;
    }
    temp=_block2.center;
    if(temp.y>700.0)
    {//checking if block2 has reached bottom of screen
        bl2.y=20.0-(r*100);
        _block2.center=bl2;
    }
   if((_runnerleft.center.y-temp.y)<=20 && _runnerleft.center.x<=230 && _runnerleft.center.x>=140)
    {// collision checking of runner and block2
        _textscore.hidden=NO;
        _textscore.text=string;
        [time invalidate];
    [time1 invalidate];
    [time2 invalidate];
        [score invalidate];
         _playagain.hidden=NO;
    }
    temp=_block3.center;
    if(temp.y>700.0)
    {//checking if block3 has reached bottom of screen
        bl3.y=20.0-(r*100);
        _block3.center=bl3;
    }
   if((_runnerleft.center.y-temp.y)<=20 && _runnerleft.center.x<=340 && _runnerleft.center.x>=250)
    {// collision checking of runner and block3
        _textscore.hidden=NO;
       _textscore.text=string;
        [time invalidate];
        [time1 invalidate];
        [time2 invalidate];
        [score invalidate];
         _playagain.hidden=NO;
    }
    temp1=_fire.center;
    temp2=_block1.center;
    
    //fire checking
    if(b==1)
    {
        if((_fire.center.y-_block1.center.y)<=-10.0 && _runnerleft.center.x<=110 && _runnerleft.center.x>=20)
        {
            _explosion.center=_block1.center;
            _explosion.hidden=NO;
            bl1.y=20.0-(r*100);
            sleep(0.5);
            _block1.center=bl1;
            _fire.hidden=YES;
            //_explosion.hidden=YES;
            b=0;
        }
        else if((_fire.center.y-_block2.center.y)<=-10.0 && _runnerleft.center.x<=230 && _runnerleft.center.x>=140)
        {
            _explosion.center=_block2.center;
            _explosion.hidden=NO;
            sleep(0.5);
            bl2.y=20.0-(r*100);
            _block2.center=bl2;
             _fire.hidden=YES;
            //_explosion.hidden=YES;
            b=0;

        }
        else if((_fire.center.y-_block3.center.y)<=-10.0  && _runnerleft.center.x<=340 && _runnerleft.center.x>=250)
        {
            _explosion.center=_block3.center;
            _explosion.hidden=NO;
            sleep(0.5);
            bl3.y=20.0-(r*100);
            _block3.center=bl3;
            _fire.hidden=YES;
         //   _explosion.hidden=YES;
            b=0;
        }
    }

}

-(void)movingblock:timer
{
    bl1.y=bl1.y+30.0;
    bl2.y=bl2.y+30.0;
    bl3.y=bl3.y+30.0;

    _block1.center=bl1;
    _block2.center=bl2;
    _block3.center=bl3;
}
-(void)hhh:timer
{
    _runner.hidden=YES;
   if(a==1)
   {
       _runnerleft.hidden=NO;
       _runnerright.hidden=YES;
       a=0;
   }
    else
    {
        _runnerleft.hidden=YES;
        _runnerright.hidden=NO;
        a=1;
    }
}

-(void)updatescore:timer
{
    count=count+1;
}

@end
